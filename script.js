/*******ENVIAR MENSAGEM*********/

function enviarMensagem(){

    let input = document.querySelector(".input-mensagem");

    let mensagens = document.createElement("div");
    mensagens.classList.add("mensagens")

    let mensagem = document.createElement("div");
    mensagem.classList.add("mensagem")

    let conteudoMensagem = document.createElement("p");
    mensagem.classList.add("conteudo-mensagem");
    conteudoMensagem.innerText = input.value

    let botoes = document.createElement("div")
    botoes.classList.add("botoes")


    let btnEditar = document.createElement("button");
    btnEditar.classList.add("btn-editar")
    btnEditar.innerText = "Editar"

    let btnExcluir = document.createElement("button");
    btnExcluir.classList.add("btn-msg")
    btnExcluir.innerText = "Excluir"
    btnExcluir.setAttribute("onclick", "this.parentNode.parentNode.remove()")



    /**********CRIAR ELEMENTO NO HTML ***********/
    let escopoMensagem = document.querySelector(".escopo-mensagem")
    escopoMensagem.append(mensagens)
    mensagens.append(mensagem)
    mensagens.append(botoes)
    mensagem.append(conteudoMensagem)
    botoes.append(btnEditar)
    botoes.append(btnExcluir)



}

let btnEnviar = document.querySelector(".btn-enviar");
btnEnviar.addEventListener("click", enviarMensagem)
